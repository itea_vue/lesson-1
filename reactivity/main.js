class MyVue {
    constructor(params) {
        this.rootSelector = params.elem;
        this.data = params.data;

        this.init()
    }

    init() {
        const vm = this;

        this.rootElement = document.querySelector(this.rootSelector);

        for(let key in this.data) {
            this[key] = this.data[key];

            let val = this[key];

            Object.defineProperty(this, key, {
                get() {
                    return val
                },
                set(newVal) {
                    val = newVal;
                    vm.render();
                }
            })
        }
        this.render();
    }

    render() {
        if(!this.newElem) {
            this.newElem = document.createElement('div');
        }

        this.newElem.innerHTML = `
            <span> ${this.str}  </span>
        `;

        this.rootElement.appendChild(this.newElem);
    }
}



document.addEventListener('DOMContentLoaded', main, false);

function main() {
    const test = new MyVue({
        elem: '#root',
        data: {
            str: ''
        }
    });

    const inp = document.querySelector('#root input');
    inp.addEventListener('input', e => {
        e.preventDefault();
        test.str = e.target.value
    })

}

// реактивность в Vue.js https://webdevblog.ru/ponimanie-reaktivnosti-vo-vue-js-shag-za-shagom/
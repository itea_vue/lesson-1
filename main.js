new Vue({
    el: '#root',
    data: {
        testIf: 1,
        testInter: 'интерполяция',
        testBind: true,
        testBindClassName: 'my-btn',
        testFor: [
            'vue', 'react', 'angular'
        ],
        testForObject: {
            test: '-vue',
            test1: '-react',
            test2: '-angular',
        },
        testOn: 'mySecBtn',

        themeColors: {
            red: 'red',
            blue: 'blue',
            orange: 'orange',
            white: 'white'

        },
        curTheme: 'white'
    }
})

// const myArr = [1, 2, 3];
//
// const arrayNumInc = (arr, inc = 1) => {
//     let output = [];
//
//     for(let i = 0; i < arr.length; i++) {
//         output.push(arr[i] + imc)
//     }
//     return output
// }
//
// const arrayNumIncMap = (arr, inc = 1) => arr.map(item => item + inc)
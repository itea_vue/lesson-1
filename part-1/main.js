
new Vue({
    el: '#root',
    data: {
        num: 1,
        num2: 2,
        testText: 'hello',
        testHtml: '<h1>hello html</h1>',
        customAttr: {
                name: 'type',
                value: 'button'
        },
        testModel: '',
        compArr: [
            'Anton', 'Oleg', 'Artur', 'Igor'
        ]
    },

    computed: {

        filteredCompArr() {
            return this.compArr.filter(name => name[0] === "A")
        }
    },
    methods: {
        clickHandler() {
            console.log('[methods]', this.num)
        },

        customMeth() {
            this.clickHandler()
        }
    }
})
